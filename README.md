# Healthy Groceries

Website:    
    https://www.healthygroceries.me/     
Postman Documentation:    
    https://documenter.getpostman.com/view/12882847/TVewajan     


### Names:    
    Drake Liang  
    Warren Wang  
    Abhi Dhir  
    Carter Chu  
    Stephen Zheng  

### GitLabID:  
    @drakeliang  
    @warren.wang1  
    @dhir.abhimanyu  
    @carterchu1  
    @stephenzshu 

### Git SHA: 1a0728bcd3a071e34857902a02a154de95ddf9bd

### Project Leader Phase 4: 
    Stephen Zheng

### Pipeline:  
    https://gitlab.com/drakeliang/healthy-groceries/-/pipelines

### Website: 
    healthygroceries.me

### Est Time to Completion:  
    Drake Liang:  24 hours
    Warren Wang:  34 hours
    Abhi Dhir:  24 hours  
    Carter Chu:  24 hours
    Stephen Zheng:  20 hours

### Actual Completion Time:  
    Drake Liang:  30 hours
    Warren Wang:  30 hours
    Abhi Dhir:  28 hours
    Carter Chu:   24 hours
    Stephen Zheng:  20 hours

### Comments:
