import sqlalchemy as db

## Fill in password and host address ##
password = ""
host = ""
engine = db.create_engine(f"postgresql://postgres:{password}@{host}:1234/postgres")

with engine.connect() as connection:
    metadata = db.MetaData()

    ## Sample code to create a table ##
    test = db.Table(
        "test", metadata, db.Column("Id", db.Integer()), db.Column("name", db.VARCHAR())
    )

    """
    metadata.create_all(engine)
    """

    ## Sample code for inserting data or running queries on the data ##
    """
    One insert at a time
    query = db.insert(test).values(Id=1, name='test-user') 
    ResultProxy = connection.execute(query)

    Inserting many records at ones
    query = db.insert(test) 
    values_list = [{'Id':'2', 'name':'a'},
                {'Id':'3', 'name':'b'}]
    ResultProxy = connection.execute(query,values_list)
    """

    ## Querying ##
    """
    metadata = db.MetaData()
    test = db.Table('test', metadata, autoload=True, autoload_with=engine)
    query = db.select([test])
    ResultProxy = connection.execute(query)
    print(ResultProxy.fetchall())
    """
