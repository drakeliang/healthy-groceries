import json

files = []
food = [
    "cheese",
    "apple",
    "bread",
    "steak",
    "celery",
    "candy",
    "hamburger",
    "fish",
    "watermelon",
    "chocolate",
    "cookies",
    "egg",
    "rice",
    "coke",
    "coffee",
]
for f in food:
    with open(f + ".json", "r") as file:
        files.append(json.load(file))

foodGroups = [
    "Margarine & Cheese",
    "Fruits",
    "Wheat",
    "Luncheon (Processed) Meats",
    "Vegetables",
    "Sweets",
    "Fast Foods",
    "Fish",
    "Fruits",
    "Chocolate",
    "Baked Goods",
    "Egg",
    "White Rice",
    "Soft Drinks",
    "Caffeine",
]
conditionConsume = [1, 2, 3, 5, 8, 21, 23, 207, 28, 31, 33, 37, 41, 55, 10]

fileIndex = 0
res = []
index = 0
for file in files:
    for data in file["hints"]:
        food_data = data["food"]
        if "image" in food_data and "FIBTG" in food_data["nutrients"]:
            res.append(
                {
                    "id": index,
                    "name": food_data["label"],
                    "calories": food_data["nutrients"]["ENERC_KCAL"],
                    "protein": food_data["nutrients"]["PROCNT"],
                    "fat": food_data["nutrients"]["FAT"],
                    "carbs": food_data["nutrients"]["CHOCDF"],
                    "fiber": food_data["nutrients"]["FIBTG"],
                    "food group": foodGroups[fileIndex],
                    "consume": conditionConsume[fileIndex],
                    "image": food_data["image"],
                }
            )
            index += 1
        # else:
        # 	res.append({'id' : food_data['foodId'], 'name' : food_data['label'], 'nutrients' : food_data['nutrients'], 'food group' : foodGroups[fileIndex]})
    fileIndex += 1

with open("database.json", "w") as file:
    json.dump(res, file, indent=4)
