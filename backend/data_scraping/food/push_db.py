import sqlalchemy as db
import json

## Fill in password and host address ##
password = "postgres"
host = "groceries-db.calaqhjpcqx4.us-east-1.rds.amazonaws.com"
engine = db.create_engine(f"postgresql://postgres:{password}@{host}:1234/postgres")


with open("database.json", "r") as file:
    data = json.load(file)

with engine.connect() as connection:
    metadata = db.MetaData()
    table = db.Table(
        "FOOD",
        metadata,
        db.Column("id", db.INTEGER()),
        db.Column("name", db.VARCHAR()),
        db.Column("calories", db.FLOAT()),
        db.Column("protein", db.FLOAT()),
        db.Column("fat", db.FLOAT()),
        db.Column("carbs", db.FLOAT()),
        db.Column("fiber", db.FLOAT()),
        db.Column("food group", db.VARCHAR()),
        db.Column("consume", db.INTEGER()),
        db.Column("image", db.VARCHAR()),
    )
    metadata.create_all(engine)
    query = db.insert(table)
    res = connection.execute(query, data)
    # table.drop(engine)

    query = db.select([table])
    res = connection.execute(query)
    print(res.fetchall())
