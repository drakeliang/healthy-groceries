import requests
import json
import sys

# def jprint(obj):
#     # create a formatted string of the Python JSON object
#     text = json.dumps(obj, sort_keys=True, indent=4)
#     print(text)

orig_stdout = sys.stdout
f = open("out.txt", "w")
sys.stdout = f

query = "coffee"
response = requests.get(
    "https://api.edamam.com/api/food-database/v2/parser?ingr="
    + query
    + "&app_id=0ec60e5e&app_key=65ac7a474c9471611bfacdd8984abfc2"
)
data = response.json()
# response = requests.get("https://api.nal.usda.gov/fdc/v1/foods/search?api_key=ZT7hvTqWaa5b9wx8EchQn1KSoHZyeynobEH3C86c&query=pie")
# response = requests.get("https://api.nal.usda.gov/fdc/v1/foods/list?api_key=ZT7hvTqWaa5b9wx8EchQn1KSoHZyeynobEH3C86c")
print(data)
# jprint(response.json())
# jprint(data)

with open(query + ".json", "w") as write_file:
    json.dump(data, write_file, indent=4)

sys.stdout = orig_stdout
f.close()
