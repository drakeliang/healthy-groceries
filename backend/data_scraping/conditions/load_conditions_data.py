import requests
import json


api_path = "https://api.nutridigm.com/api/v1/nutridigm/"


def load_health_conditions():
    pass


def load_consume():
    path = api_path + "topitemstoconsume"
    base_params = {"subscriptionId": "0"}

    with open("conditions_from_site.json") as file:
        conditionData = json.load(file)

    ret = {}
    for condition in conditionData:
        params = {"problemId": condition["problemID"], **base_params}

        response_get = requests.get(path, params=params)
        data = response_get.json()

        ret[condition["problemID"]] = data[0]

    with open("consume_data_raw.json", "w") as file:
        json.dump(ret, file)


def load_avoid():
    path = api_path + "topitemstoavoid"
    base_params = {"subscriptionId": "0"}

    with open("conditions_from_site.json") as file:
        conditionData = json.load(file)

    ret = {}
    for condition in conditionData:
        params = {"problemId": condition["problemID"], **base_params}

        response_get = requests.get(path, params=params)
        data = response_get.json()

        ret[condition["problemID"]] = data[0]

    with open("avoid_data_raw.json", "w") as file:
        json.dump(ret, file)


load_consume()
load_avoid()
