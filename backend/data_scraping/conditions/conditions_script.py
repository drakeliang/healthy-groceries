import json
import pandas as pd
from collections import defaultdict
import numpy as np

df = pd.read_excel("hcc_map.xlsx")
# df = df.iloc[:, [1, 4]]

with open("avoid_data.json", "r") as file:
    conditionData = json.load(file)
    conditionSet = {int(x) for x in conditionData}

# print(df.head())
# exit()

codeToName = defaultdict(list)
# maps = [set(), set(), set(), set()]
for i in range(df.shape[0]):
    name = df.iloc[i, 1]
    code = df.iloc[i, 4]
    if not np.isnan(code):
        code = int(round(code))
        if code in conditionSet:
            codeToName[code].append(name)

print(len(conditionSet))

print(codeToName)
