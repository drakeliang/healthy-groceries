import json

with open("code_to_name.json", "r") as file:
    code_to_name = json.load(file)
with open("avoid_data_clean.json", "r") as file:
    avoid_data = json.load(file)
with open("consume_data_clean.json", "r") as file:
    consume_data = json.load(file)

idx = 1
res = []
for code, name in code_to_name.items():
    res.append(
        {
            "id": idx,
            "code": code,
            "name": name,
            "avoid": avoid_data[code],
            "consume_data": consume_data[code],
        }
    )
    idx += 1

with open("database_like.json", "w") as file:
    json.dump(res, file, indent=4)
