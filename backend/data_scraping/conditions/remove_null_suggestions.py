import json


def go(inpath, outpath):
    with open(inpath, "r") as infile:
        data = json.load(infile)
        data = {x[0]: x[1] for x in data.items() if "yet curated" not in x[1]}
    with open(outpath, "w") as outfile:
        json.dump(data, outfile)


go("avoid_data_raw.json", "avoid_data.json")
go("consume_data_raw.json", "consume_data.json")
