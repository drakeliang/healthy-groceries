from unittest import main, TestCase
import main as api


class MyTestCase2(TestCase):
    def test_conditions_all(self):
        res = api.get_limit_offset(api.conditions_table, api.conditions_cols, 100000, 0)
        self.assertEqual(len(res), 109)

    def test_conditions_offset(self):
        res = api.get_limit_offset(api.conditions_table, api.conditions_cols, 100000, 5)
        self.assertEqual(len(res), 109 - 5)

    def test_conditions_limit(self):
        res = api.get_limit_offset(api.conditions_table, api.conditions_cols, 16, 5)
        self.assertEqual(len(res), 16)

    def test_conditions_id(self):
        res = api.get_id(api.conditions_table, api.conditions_cols, 1)
        self.assertEqual(res["id"], 1)
        self.assertEqual(res["name"], "High cholesterol")

    def test_foods_all(self):
        res = api.get_limit_offset(api.foods_table, api.foods_cols, 100000, 0)
        self.assertEqual(len(res), 124)

    def test_foods_offset(self):
        res = api.get_limit_offset(api.foods_table, api.foods_cols, 100000, 5)
        self.assertEqual(len(res), 124 - 5)

    def test_foods_limit(self):
        res = api.get_limit_offset(api.foods_table, api.foods_cols, 50, 3)
        self.assertEqual(len(res), 50)

    def test_foods_id(self):
        res = api.get_id(api.foods_table, api.foods_cols, 5)
        self.assertEqual(res["id"], 5)
        self.assertEqual(res["name"], "Ella's Kitchen Apples Apples Apples")

    def test_stores_all(self):
        res = api.get_limit_offset(api.stores_table, api.stores_cols, 100000, 0)
        self.assertEqual(len(res), 100)

    def test_stores_offset(self):
        res = api.get_limit_offset(api.stores_table, api.stores_cols, 100000, 5)
        self.assertEqual(len(res), 100 - 5)

    def test_stores_limit(self):
        res = api.get_limit_offset(api.stores_table, api.stores_cols, 7, 5)
        self.assertEqual(len(res), 7)

    def test_stores_id(self):
        res = api.get_id(api.stores_table, api.stores_cols, 12)
        self.assertEqual(res["id"], 12)
        self.assertEqual(res["locationId"], "02580515")
        self.assertEqual(res["chain"], "SHELL COMPANY")


if __name__ == "__main__":
    main()
