from flask import Flask, render_template, request, send_from_directory
from flask_restful import Resource, Api, reqparse
import sqlalchemy as db
from sqlalchemy import and_, text
from sqlalchemy.sql import select, func
from flask_cors import CORS
import os, json

app = Flask(
    __name__,
    static_folder="../../frontend/build/static",
    template_folder="../../frontend/build",
)
CORS(app)
api = Api(app)

# get environment variables
import os

if not os.getenv("PROD"):
    from dotenv import load_dotenv

    load_dotenv(".env")

password = os.getenv("POSTGRES_PASSWORD")
host = os.getenv("POSTGRES_HOST")

# database engine
engine = db.create_engine(f"postgresql://postgres:{password}@{host}:1234/postgres")
metadata = db.MetaData()

# create tables
stores_cols = [
    "id",
    "locationId",
    "chain",
    "address",
    "geolocation",
    "name",
    "departments",
]
stores_table = db.Table("stores", metadata, autoload=True, autoload_with=engine)

foods_cols = [
    "id",
    "name",
    "calories",
    "protein",
    "fat",
    "carbs",
    "fiber",
    "food group",
    "consume",
    "image",
]
foods_table = db.Table("FOOD", metadata, autoload=True, autoload_with=engine)

conditions_cols = [
    "id",
    "code",
    "name",
    "avoid",
    "consume",
    "cases",
    "influence",
    "media",
]
conditions_table = db.Table("CONDITIONS", metadata, autoload=True, autoload_with=engine)

metadata.create_all(engine)

# make a query and fetch all
def query_all(query):
    with engine.connect() as connection:
        res = connection.execute(query).fetchall()
    return res


# turn db row into dict with col name mapping to value
def to_dict(tpl, cols):
    assert len(cols) == len(tpl)
    return {k: v for k, v in zip(cols, tpl)}


# query table with a limit and offset
def get_limit_offset(table, cols, limit, offset):
    query = table.select().order_by(table.c.id).limit(limit).offset(offset)
    res = query_all(query)
    res = [to_dict(x, cols) for x in res]
    return res


# query table for an id
def get_id(table, cols, id_):
    id_ = int(id_)
    query = table.select().where(table.c.id == id_)
    res = query_all(query)
    assert len(res) <= 1
    res = [to_dict(x, cols) for x in res]
    return res[0] if res else None


# aggregate store, food, and condition into one object
# {'store': [stores], 'food': [foods], 'condition': [conditions]}
class EverythingList(Resource):
    def get(self):
        args = request.args

        store_limit = query_all(func.count(stores_table.c.id))[0][0]
        food_limit = query_all(func.count(foods_table.c.id))[0][0]
        condition_limit = query_all(func.count(conditions_table.c.id))[0][0]

        return {
            "store": get_limit_offset(stores_table, stores_cols, store_limit, 0),
            "food": get_limit_offset(foods_table, foods_cols, food_limit, 0),
            "condition": get_limit_offset(
                conditions_table, conditions_cols, condition_limit, 0
            ),
        }


# transform a store object
# add hours field
# flatten the jsons in the address and geolocation fields
def trans_store(x):
    x["hours"] = "6:00 - 23:59" if x["chain"] == "KROGER" else "24 Hours"
    toFlatten = ["address", "geolocation"]
    for field in toFlatten:
        x.update(json.loads(x[field].replace("'", '"')))
        del x[field]


# resources for each model
# a list (with limit and offset) of model objects
# a single model object with the given id


class StoresList(Resource):
    def get(self):
        args = request.args
        limit = (
            int(args["limit"])
            if "limit" in args
            else query_all(func.count(stores_table.c.address))[0][0]
        )
        offset = int(args["offset"]) if "offset" in args else 0
        res = get_limit_offset(stores_table, stores_cols, limit, offset)
        for x in res:
            trans_store(x)
        return res


class Store(Resource):
    def get(self, store_id):
        x = get_id(stores_table, stores_cols, store_id)
        trans_store(x)
        return x


# get the hours for a given store
class StoreHours(Resource):
    def get(self, store_id):
        try:
            departments = (
                get_id(stores_table, stores_cols, store_id)["departments"]
                .replace("r's", "rs")
                .replace("'", '"')
                .replace("True", '"True"')
                .replace("False", '"False"')
            )
            result = {
                d["name"]: d["hours"] for d in json.loads(departments) if d.get("hours")
            }
            return result
        except:
            return {}

food_shell = [0, 1, 2, 44, 45, 46, 47, 48, 81, 89, 90, 91, 92, 93, 94, 95, 96, 97, 105, 112, 113, 114, 115, 117, 118, 119, 120, 121, 122, 123]

# transform food
# map foods to stores
def trans_food(food):
    if food is None:
        return
    if food["id"] in food_shell:
        food["store"] = "kroger, shell"
    else:
        food["store"] = "kroger"


class FoodsList(Resource):
    def get(self):
        args = request.args
        limit = (
            int(args["limit"])
            if "limit" in args
            else query_all(func.count(foods_table.c.id))[0][0]
        )
        offset = int(args["offset"]) if "offset" in args else 0
        res = get_limit_offset(foods_table, foods_cols, limit, offset)
        for x in res:
            trans_food(x)
        return res


class Food(Resource):
    def get(self, food_id):
        res = get_id(foods_table, foods_cols, food_id)
        trans_food(res)
        return res


group_food_avoid = {
    "Sweets": [44, 45, 46],
    "Luncheon (Processed) Meats": [22, 33, 35],
    "Margarine & Cheese": [1, 2, 3],
    "Fast Foods": [49, 58, 65],
}

condition_kroger = [33, 62, 63, 68, 69, 93, 97, 98, 99, 101, 102, 104, 105, 106, 108]

# transform condition
# make foods_to_avoid field with a list of food ids of example foods to avoid for the given condition
# map conditions to stores
def trans_condition(condition):
    if condition is None:
        return
    foods_to_avoid = []
    for group, food_ids in group_food_avoid.items():
        if group in condition["avoid"]:
            foods_to_avoid.append(food_ids[condition["id"] % len(food_ids)])
    condition["foods_to_avoid"] = str(foods_to_avoid)
    if condition["id"] in condition_kroger:
        condition["store"] = "kroger"
    else:
        condition["store"] = "kroger, shell"


class ConditionsList(Resource):
    def get(self):
        args = request.args
        limit = (
            int(args["limit"])
            if "limit" in args
            else query_all(func.count(conditions_table.c.id))[0][0]
        )
        offset = int(args["offset"]) if "offset" in args else 0
        res = get_limit_offset(conditions_table, conditions_cols, limit, offset)
        for x in res:
            trans_condition(x)
        return res


class Condition(Resource):
    def get(self, condition_id):
        res = get_id(conditions_table, conditions_cols, condition_id)
        trans_condition(res)
        return res


# boilerplate


@app.route("/images/<image_name>")
def image(image_name):
    return send_from_directory(os.path.join(app.template_folder, "images"), image_name)


@app.route("/favicon.ico")
def fav():
    return send_from_directory(app.template_folder, "favicon.ico")


# for deployment
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")


# api.add_resource(Empty, '/')

# add resources
api.add_resource(StoresList, "/api/stores")
api.add_resource(Store, "/api/stores/<store_id>")
api.add_resource(StoreHours, "/api/storeHours/<store_id>")
api.add_resource(FoodsList, "/api/foods")
api.add_resource(Food, "/api/foods/<food_id>")
api.add_resource(ConditionsList, "/api/conditions")
api.add_resource(Condition, "/api/conditions/<condition_id>")
api.add_resource(EverythingList, "/api/everything")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
