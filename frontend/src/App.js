import React from 'react';
import './App.css';
import {
    Route,
    HashRouter
} from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/about/About";
import Stores from "./pages/storePages/Stores";
import Store from "./pages/storePages/Store";
import Foods from "./pages/foodPages/Foods";
import Food from "./pages/foodPages/Food";
import Conditions from "./pages/healthPages/Conditions";
import Condition from "./pages/healthPages/Condition";
import NavBar from "./pages/NavBar"
import Search from "./pages/Search"
import Visualizations from "./pages/visualizations/Visualizations"

require('dotenv').config();

export default function App() {

    const defaultSearchResult = (
        <div className="content">
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={About} />
            <Route exact path="/grocery" component={Stores} />
            <Route path="/grocery/:groceryId" component={Store} />
            <Route exact path="/food" component={Foods} />
            <Route path="/food/:foodId" component={Food} />
            <Route exact path="/health" component={Conditions} />
            <Route path="/health/:conditionId" component={Condition} />
            <Route path="/search/:query" component={Search} />
            <Route exact path="/visualizations" component={Visualizations} />
        </div>
    )

    return (
        <HashRouter>
            <div>
                <h1 style={{ textAlign: 'left' }}>
                    <a href="/" style={{ color: 'darkgreen' }}><img src="/favicon.ico" alt="icon" style={{ height: '1em' }} />Healthy Groceries</a>
                </h1>
                <ul className="header">
                    <NavBar />
                </ul>
                <div>{defaultSearchResult}</div>
            </div>
        </HashRouter>
    )
}
