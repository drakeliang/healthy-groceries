import { ToFuse, Sort, Reverse, Slice, Compose } from './Transforms'
import Fuse from 'fuse.js';

 /**
  * Applies transforms to the grid data.
  * Used by the grid components, see them for what the parameters should be.
  * 
  * @param {object} data raw data
  * @param {string} query search query string, empty if no search
  * @param {[string]} fuseKeys list of fields to search in
  * @param {[OneFilter]} filters list of filters to use
  * @param {string} sortField field to sort by, empty if not sorting
  * @param {boolean} sortAsc true if sorting ascending, false is sorting descending
  * @param {integer} page page number
  * @param {integer} pageSize number of instances to put on one page
  * 
  * @return {integer} maximum value for page
  * @return {Fuse object} resulting data
  */
function applyTransforms(data, query, fuseKeys, filters, sortField, sortAsc, page, pageSize) {
    // search engine
    const conditionFuse = new Fuse(data, {
        keys: fuseKeys,
        includeMatches: true,
        minMatchCharLength: 2,
        threshold: 0.3
    });
    // either search data or use the entire dataset
    const rawData = query ? conditionFuse.search(query) : JSON.parse(JSON.stringify(data));
    
    // build a sequential array of transforms
    var transforms = [];

    // if used full data, transform to a fuse object
    if (!query) {
        transforms.push(ToFuse());
    }

    // start will all the filters
    filters.forEach((filter) => {
        transforms.push(filter.buildTransform());
    });

    // if sorting, sort, and then reverse if descending
    if (sortField) {
        transforms.push(Sort(sortField));
        if (!sortAsc) {
            transforms.push(Reverse());
        }
    }

    // compose the transforms
    const transformedData = Compose(transforms)(rawData);

    // with the transformed data, compute max pages
    const maxPages = Math.floor(transformedData.length / pageSize) + (transformedData.length % pageSize !== 0);

    // take the slice for the current page
    const slicedData = Slice((page - 1) * pageSize, page * pageSize)(transformedData);

    return [maxPages, slicedData];
}

export { applyTransforms };