import { FilterType, Filter, Identity } from './Transforms'

/**
  * Class to keep track of a single filter, including its field, operator, and target
  * 
  * @property {function} setFilterFlag function to call to force a rerender of the parent UI component
  * @property {string} field field to access in data, for first operand
  * @property {FilterType} op operator in comparison
  * @property {any} target second operand
  */
 class OneFilter {
    /**
     * @param {function} setFilterFlag setFilterFlag
     * @param {string} field field
     * @param {FilterType} defaultOp default operator to use
     */
    constructor(setFilterFlag, field, defaultOp = FilterType.EQ) {
        this.setFilterFlag = setFilterFlag;
        this.field = field;
        this.op = defaultOp;
        this.target = undefined;
    }
    /**
     * Set operator and rerender.
     * 
     * @param {FilterType} op operator to set
     */
    setOp(op) {
        this.op = op;
        this.setFilterFlag(Math.random());
    }

    /**
     * Set target and rerender.
     * 
     * @param {any} target target to set
     */
    setTarget(target) {
        this.target = target;
        this.setFilterFlag(Math.random());
    }

    /**
     * Remove target.
     */
    removeTarget() {
        this.setTarget(undefined);
    }

    /**
     * Get operator.
     * 
     * @return {FilterType} operator
     */
    getOp() {
        return this.op;
    }

    /**
     * Get target.
     * 
     * @return {any} target
     */
    getTarget() {
        return this.target;
    }

    /**
     * Get field.
     * 
     * @return {string} field
     */
    getField() {
        return this.field;
    }

    /**
     * Return a transform to represent the filter.
     * If either op or target is undefined, it is considered a no-op and the identify transform will be returned.
     * 
     * @return {Transform} transform
     */
    buildTransform() {
        if (this.op === undefined || this.target === undefined) {
            return Identity();
        } else {
            return Filter(this.field, this.op, this.target);
        }
    }
}

export { OneFilter }