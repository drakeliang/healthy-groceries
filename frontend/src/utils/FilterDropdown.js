import React from 'react';
import { Dropdown, DropdownButton, InputGroup } from 'react-bootstrap'

 /**
  * Creates a dropdown react component for users to select the operator and target for the filter.
  * 
  * @param {string} field field to filter by
  * @param {OneFilter} filter filter object to set and get the filter state
  * @param {string} optOutValue display string when opting out of a filter
  * @param {[any]} targets list of targets
  * @param {[FilterType]} ops list of operators
  * @param {boolean} showOp whether to show the op dropdown, false if one possible operator that is implied
  * 
  * @return {React component} resulting dropdown component
  */
function FilterDropdown(field, filter, optOutValue, targets, ops, showOp) {
    return (
        <>
            <InputGroup.Prepend>
                <InputGroup.Text>{field}</InputGroup.Text>
            </InputGroup.Prepend>
            {showOp && <DropdownButton title={filter.getOp()} as={InputGroup.Prepend} variant="outline-secondary">
                {
                    ops.map((x) => (<Dropdown.Item onClick={() => { filter.setOp(x) }}>{x}</Dropdown.Item>))
                }
            </DropdownButton>}
            <DropdownButton title={filter.getTarget() || optOutValue} as={InputGroup.Prepend} variant="outline-secondary">
                <Dropdown.Item onClick={() => { filter.removeTarget(); }}>{optOutValue}</Dropdown.Item>
                {
                    targets.map((x) => (<Dropdown.Item onClick={() => { filter.setTarget(x); }}>{x}</Dropdown.Item>))
                }
            </DropdownButton>
        </>
    )
}

export { FilterDropdown }