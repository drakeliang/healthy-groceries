 /**
  * Transforms utility.
  * 
  * Fuse array is defined as an array where the values are Fuse items
  * Fuse items are objects that contain the fields 'item' and 'matches'
  * 'item' is an object that represents the data
  * 'matches' is an array that stores matches
  * 
  * Each of these symbols is a function that takes in some parameters and returns a transform.
  * The transform is a function that takes in a Fuse array and returns a new Fuse array with the transform applied.
  * The original array is not changed.
  * 
  * Example use with T as a transform;
  * var trans = T();
  * var data = getData();// start with data
  * data = trans(data);
  * 
  * Use Compose(transforms) to compose a list of transforms sequentially.
  */

 /**
  * Changes value into lowercase if it is a string.
  * 
  * @param {any} x input
  * 
  * @return {any} same as x but lowercase if x is string 
  */
 function lower(x) {
    if (typeof(x) === "string") {
        x = x.toLowerCase();
    }
    return x;
}

/**
 * Get the lowercase of the field property of a Fuse item.
 * 
 * @param {Fuse item} x data structure to get value from
 * @param {string} field name of field to get
 * 
 * @return {any} the lowercase version of the value in the 'field' field of x
 */
function getLower(x, field) {
    return lower(x.item[field]);
}

/**
 * Enum of filter operations.
 */
const FilterType = {
    LT: '<',
    LTE: '<=',
    EQ: '==',
    GTE: '>=',
    GT: '>',
    IN: 'in',
    DEQ: 'deq'
}

/**
 * Gets a binary predicate to use in filtering in the form of a lambda.
 * 
 * @param {string} field name of the field of the data to get values from and use as first operand
 * @param {FilterType} type FilterType value that represents the binary operation to compare the value and the target
 * @param {any} target value to use as the second operand
 * 
 * @return {(Fuse item) => boolean} lambda that will take a Fuse item as an input and return a boolean representing whether to keep it in the filter
 */
function getFilterPredicate(field, type, target) {
    target = lower(target);
    switch (type) {
        case FilterType.LT:
            return (x) => getLower(x, field) < target;
        case FilterType.LTE:
            return (x) => getLower(x, field) <= target;
        case FilterType.EQ:
            return (x) => getLower(x, field) === target;
        case FilterType.GTE:
            return (x) => getLower(x, field) >= target;
        case FilterType.GT:
            return (x) => getLower(x, field) > target;
        case FilterType.IN:
            // parse the field as JSON which will produce a list of strings
            // true if the target is an element of the list
            return (x) => JSON.parse(getLower(x, field).replace(/'/g, '"')).includes(target);
        case FilterType.DEQ:
            // true if the value contains target as a substring
            return (x) => getLower(x, field).includes(target);
        default:
            // input is an unsupported filter type
            return undefined;
    }
}

 /**
  * Special case.
  * Transforms a normal data array into a Fuse array.
  * 
  * @return {(array) => Fuse array} resulting transformation
  */
 const ToFuse = () => {
    return (data) => {
        return data.map(x => ({
            item: x,
            matches: [""],
            score: 1
        }))
    }
}

/**
 * Sorts the data by the values in a given field.
 * 
 * @param {string} field name of field
 * 
 * @return {(Fuse array) => Fuse array} resulting transformation
 */
const Sort = (field) => {
    return (data) => {
        return data.slice().sort((x1, x2) => {
            var a1 = getLower(x1, field);
            var a2 = getLower(x2, field);
            return (a1 > a2) - (a2 > a1);
        });
    }
}

/**
  * Reverses the data
  * 
  * @return {(Fuse array) => Fuse array} resulting transformation
  */
const Reverse = () => {
    return (data) => {
        return data.slice().reverse();
    }
}

/**
 * Filters the data by only keeping items where the operation comparing the 'field' field's value to the target yields true.
 * Equivalent to 'operator(x.item[field], target)'.
 * 
 * @param {string} field name of the field of the data to get values from and use as first operand
 * @param {FilterType} type FilterType value that represents the binary operation to compare the value and the target
 * @param {any} target value to use as the second operand
 * 
 * @return {(Fuse array) => Fuse array} resulting transformation
 */
const Filter = (field, type, target) => {
    const cmp = getFilterPredicate(field, type, target);
    return (data) => {
        return data.filter(cmp);
    }
}

/**
 * Slices the data into a subarray of it.
 * 
 * @param {integer} start inclusive starting index
 * @param {integer} end exclusive ending index
 * 
 * @return {(Fuse array) => Fuse array} resulting transformation
 */
const Slice = (start, end) => {
    return (data) => {
        return data.slice(start, end);
    }
}

/**
 * Does nothing; identity function.
 * 
 * @return {(Fuse array) => Fuse array} resulting transformation
 */
const Identity = () => {
    return (data) => {
        return data;
    }
}

/**
 * Creates a transform that is the composition of a array of transforms.
 * The order of the transforms will be sequential starting from the beginning of the array.
 * 
 * @param {[Transform]} transforms array of transforms to apply
 * 
 * @return {(Fuse array) => Fuse array} resulting transformation
 */
const Compose = (transforms) => {
    return (data) => {
        for (var i = 0; i < transforms.length; i++) {
            data = transforms[i](data);
        }
        return data;
    }
}

export {ToFuse, FilterType, Sort, Reverse, Filter, Slice, Identity, Compose}
