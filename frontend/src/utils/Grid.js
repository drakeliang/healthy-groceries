import React from 'react';
import { Container, Row, Col, CardDeck, Pagination, Dropdown, DropdownButton, InputGroup } from 'react-bootstrap'
import { FilterDropdown } from './FilterDropdown'

// creates a grid React component from the values produced by the Grid files
// see the specific grid files to for how the parameters should be specified
function getGrid(slicedData, filterFields, optOut, filters, filterFieldsValues,
    filterFieldsOps, filterFieldsShowOp, sortField, sortFields, sortFieldsAliases,
    setSortField, sortAsc, setSortAsc, page, setPage, maxPages, Card) {
    return (
        <>
            <Container>
                <Row>
                    <Col style={{ justifyContent: "center", display: "flex" }}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Filter by:</InputGroup.Text>
                            </InputGroup.Prepend>
                            {filterFields.map((x, i) => FilterDropdown(x, filters[i], optOut, filterFieldsValues[x], filterFieldsOps[x], filterFieldsShowOp[x]))}
                        </InputGroup>
                    </Col>
                </Row>
                <Row>
                    <Col style={{ justifyContent: "center", display: "flex" }}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Sort By:</InputGroup.Text>
                            </InputGroup.Prepend>
                            <DropdownButton title={sortField} as={InputGroup.Prepend} variant="outline-secondary">
                                <Dropdown.Item onClick={() => { setSortField("") }}>{optOut}</Dropdown.Item>
                                {sortFields.map((x) => <Dropdown.Item onClick={() => { setSortField(x) }}>{sortFieldsAliases[x]}</Dropdown.Item>)}
                            </DropdownButton>
                            <DropdownButton title={sortAsc ? "Ascending" : "Descending"} as={InputGroup.Append} variant="outline-secondary">
                                <Dropdown.Item onClick={() => setSortAsc(true)}>Ascending</Dropdown.Item>
                                <Dropdown.Item onClick={() => setSortAsc(false)}>Descending</Dropdown.Item>
                            </DropdownButton>
                        </InputGroup>
                    </Col>

                    <Col style={{ justifyContent: "center", display: "flex" }}>
                        <Pagination>
                            <Pagination.Item onClick={() => setPage(p => 1)}>{1}</Pagination.Item>
                            <Pagination.Prev onClick={() => setPage(p => Math.max(1, p - 1))}></Pagination.Prev>
                            <Pagination.Item active>{page}</Pagination.Item>
                            <Pagination.Next onClick={() => setPage(p => Math.min(maxPages, p + 1))}></Pagination.Next>
                            <Pagination.Item onClick={() => setPage(p => maxPages)}>{maxPages}</Pagination.Item>
                        </Pagination>
                    </Col>
                </Row>
            </Container>
            <br />
            <Container>
                <CardDeck style={{ flexWrap: 'wrap', justifyContent: 'center' }}>
                    {slicedData.map(Card)}
                </CardDeck>
            </Container>
        </>
    )
}

export { getGrid }