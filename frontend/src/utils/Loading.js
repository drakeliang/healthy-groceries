import React from 'react';

export default function Loading() {
    return (
        <img src="https://i.gifer.com/4V0b.gif" alt="loading" style={{display: 'block', marginLeft: 'auto', marginRight: 'auto'}}/>
    )
}