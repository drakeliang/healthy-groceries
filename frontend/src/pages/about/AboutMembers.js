import React from "react";
import { Container, Card, CardDeck } from 'react-bootstrap'

const cards = [
    {
        img: "https://miro.medium.com/max/875/1*rjrCXTaQKj8zd15MSZ4jEA.png",
        name: "Drake Liang",
        role: "Full Stack",
        bio: "A third year computer science student",
        linkedin: "/#/about"
    },
    {
        img: "https://miro.medium.com/max/704/0*BfYsqrm2MhTWsWRP.png",
        name: "Warren Wang",
        role: "Full Stack",
        bio: "I'm a computer science junior from Sugar Land, TX.",
        linkedin: "https://www.linkedin.com/in/warren-wang-742b12170/"
    },
    {
        img: "https://cs373sweblogabhidhir.files.wordpress.com/2020/08/profile-1.jpg?w=368",
        name: "Abhi Dhir",
        role: "Full Stack",
        bio: "I am a junior computer science and math major at the University of Texas at Austin.",
        linkedin: "https://www.linkedin.com/in/abhi-dhir/"
    },
    {
        img: "https://cs373f2020carterchu.files.wordpress.com/2020/10/headshot-2.jpg?w=300",
        name: "Carter Chu",
        role: "Full Stack",
        bio: "I am a junior computer science and math major at the University of Texas at Austin.",
        linkedin: "https://www.linkedin.com/in/carterchu1/"
    },
    {
        img: "https://miro.medium.com/max/875/1*D6tEdU2p9Sb2ULyt2hIoxw.jpeg",
        name: "Stephen Zheng",
        role: "Team Leader, Full Stack",
        bio: "I am a 3rd year computer science major from Sugar Land, Texas.",
        linkedin: "https://www.linkedin.com/in/stephenzshu/"
    }
]

export default function AboutMembers({ gitlabDetails }) {
    return (
        <Container>
            <CardDeck style={{ flexWrap: 'wrap', justifyContent: 'center' }}>
                {cards.map(function (card, index) {
                    return (
                        <Card style={{ minWidth: '18rem', maxWidth: '21rem' }}>
                            <Card.Img variant="top" src={card.img} style={{width: "100%", height: "auto"}} />
                            <Card.Header as="h3" style={{ textAlign: 'center' }}>{card.name}</Card.Header>
                            <Card.Body style={{ fontSize: 20 }}>
                                <Card.Subtitle >{card.role}</Card.Subtitle>
                                <Card.Text>{card.bio}</Card.Text>
                                <Card.Text>
                                    <a href={card.linkedin}>
                                        <img
                                            src="https://cdn.iconscout.com/icon/free/png-256/linkedin-154-459182.png"
                                            alt="linkedinIcon"
                                            style={{ height: '1em' }}
                                        />
                                        <u>LinkedIn</u>
                                    </a>
                                </Card.Text>
                            </Card.Body>
                            <Card.Footer as="h4">Commits: {gitlabDetails.memberCommits[index]}</Card.Footer>
                            <Card.Footer as="h4">Issues: {gitlabDetails.memberIssues[index]}</Card.Footer>
                            <Card.Footer as="h4">Unit Tests: {gitlabDetails.memberTests[index]}</Card.Footer>
                        </Card>
                    )
                })}
            </CardDeck>
        </Container>
    )
}