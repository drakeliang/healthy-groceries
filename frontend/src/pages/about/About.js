import React from "react";
import { Container } from 'react-bootstrap'
import useAxios from 'axios-hooks'
import AboutTools from './AboutTools'
import AboutMembers from './AboutMembers'
import Loading from '../../utils/Loading'

export default function About() {
    const [{ data: commitData, loading: cloading, error: cerror }] = useAxios(
        "https://gitlab.com/api/v4/projects/21390199/repository/contributors"
    )
    const [{ data: issueData, loading: iloading, error: ierror }] = useAxios(
        "https://gitlab.com/api/v4/projects/21390199/issues"
    )
    if (cloading || iloading) return <Loading />
    if (cerror || ierror) return <p>Something went wrong</p>

    const names = [
        "Drake Liang",
        "Warren Wang",
        "AbhiDhir",
        "Carter Chu",
        "Stephen Zheng"
    ]
    var gitlabDetails = {
        // to store the number of commits, issues, tests per person
        numCommits: 0,
        numIssues: 0,
        numTests: 55,
        memberCommits: [0, 0, 0, 0, 0], //ordered following names array order
        memberIssues: [0, 0, 0, 0, 0],  //ex: memberCommits[0] == Drake's commits
        memberTests: [0, 12, 0, 43, 0]
    }


    //loop through commitData and assign commits to members
    for (const i in commitData) {
        let curr = commitData[i];
        if (curr.name === names[0]) {
            gitlabDetails.memberCommits[0] += curr.commits;
        } else if (curr.name === names[1]) {
            gitlabDetails.memberCommits[1] += curr.commits;
        } else if (curr.name === names[2]) {
            gitlabDetails.memberCommits[2] += curr.commits;
        } else if (curr.name === names[3]) {
            gitlabDetails.memberCommits[3] += curr.commits;
        } else if (curr.name === names[4]) {
            gitlabDetails.memberCommits[4] += curr.commits;
        } else if (curr.name === "Drake") { //weird case
            gitlabDetails.memberCommits[0] += curr.commits;
        } else if (curr.name === "Sean Yu") { //case from carter working on sean yu's computer
            gitlabDetails.memberCommits[3] += curr.commits;
        } else if (curr.name === "stephenzshu") { //weird case
            gitlabDetails.memberCommits[4] += curr.commits;
        } else {
            console.error("Unexpected non-member commit");
        }
        gitlabDetails.numCommits += curr.commits;
    }

    // loop through issueData and assign issues to members
    for (const i in issueData) {
        let curr = issueData[i].assignees;
        for (const k in curr) {
            if (curr[k].name === names[0]) {
                gitlabDetails.memberIssues[0]++;
            } else if (curr[k].name === names[1]) {
                gitlabDetails.memberIssues[1]++;
            } else if (curr[k].name === names[2]) {
                gitlabDetails.memberIssues[2]++;
            } else if (curr[k].name === names[3]) {
                gitlabDetails.memberIssues[3]++;
            } else if (curr[k].name === names[4]) {
                gitlabDetails.memberIssues[4]++;
            } else if (curr[k].name === "Drake") { //weird case
                gitlabDetails.memberIssues[0]++;
            } else if (curr[k].name === "stephenzshu") { //weird case
                gitlabDetails.memberIssues[4]++;
            } else if (curr[k].name === "Abhi Dhir") { //weird case
                gitlabDetails.memberIssues[2]++;
            }
            gitlabDetails.numIssues++;
        }
    }

    return (
        <Container>
            <h1>About HealthyGroceries.me</h1>
            <h4 style={{ textAlign: 'left' }}> Accidentally buying foods that could exacerbate existing medical conditions is easy.&nbsp;
            HealthyGroceries.me hopes to resolve these problems and many others by collecting the relevant information into a single easy-to-use source.
                </h4>
            <h4 style={{ textAlign: 'left' }}>
                Through this site, you can determine exactly where the foods you need can be found,&nbsp;
                and which foods you should eat to help with your existing health conditions.
                </h4>
            <br />
            <div style={{
                textAlign: "center"
            }}>
                <a href="https://gitlab.com/drakeliang/healthy-groceries">
                    <img
                        src="https://www.ilovedevops.com/img/avatar-icon.png"
                        alt="linkedinIcon"
                        style={{
                            width: "300px",
                            height: "300px",
                            margin: "16px",
                            boxShadow: "5px 5px 15px rgba(0, 0, 0, 0.2)"
                        }}
                    />
                </a>
                <a href="https://documenter.getpostman.com/view/12882847/TVewajan">
                    <img
                        src="https://seeklogo.com/images/P/postman-logo-F43375A2EB-seeklogo.com.png"
                        alt="linkedinIcon"
                        style={{
                            width: "300px",
                            height: "300px",
                            margin: "16px",
                            boxShadow: "5px 5px 15px rgba(0, 0, 0, 0.2)"
                        }}
                    />
                </a>
            <h4 style={{ textAlign: 'center' }}>
                Commits: {gitlabDetails.numCommits} &nbsp;&nbsp;&nbsp;&nbsp;
                    Issues: {gitlabDetails.numIssues} &nbsp;&nbsp;&nbsp;&nbsp;
                    Unit Tests: {gitlabDetails.numTests}
            </h4>
            </div>
            <br />
            <h2 style={{ textAlign: 'center' }}> Members </h2>
            <AboutMembers gitlabDetails={gitlabDetails} />
            <br />
            <br />
            <AboutTools />
        </Container >
    );
}
