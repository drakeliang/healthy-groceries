import React from "react";
import { Col, Card } from 'react-bootstrap'

const toolsName = ["AWS Amplify:", "RDS:", "Namecheap:", "React:", "npm:", "Postman:",
    "GitLab:", "Discord:", "Bootstrap:", "Flask:", "Jest:", "Selenium:", "Fuse.js:"]
const toolsDesc = [
    " for hosting the website and API ",
    " for hosting the postgres database ",
    " registering the domain name ",
    " JavaScript library used to build the frontend of the web app, " +
    "as well as providing the functionality for dynamic content later on.",
    " JavaScript package manager ",
    " used for API design, testing, and for getting data ",
    " used for version control, testing, and issue tracking ",
    " used for communication within our group and group meetings. " +
    "A channel was set up to give notifications when events happened in GitLab",
    " JavaScript library for some frontend components.",
    " API serving",
    " Frontend Testing",
    " Frontend Testing",
    " Fuzzy Search"
]

const apiSrc = [
    "https://api.kroger.com/v1/products",
    "https://spoonacular.com/food-api",
    "https://developer.edamam.com/food-database-api-docs",
    "https://rapidapi.com/mytweetmark/api/homecook",
    "https://api.nutridigm.com/swagger/ui/index",
]
const apiText = [
    "Kroger",
    "Spoonacular",
    "Edamam",
    "Homecook",
    "Nutridigm"
]

export default function AboutTools() {
    return (
        <>
            <h2 style={{ textAlign: 'center' }}> Tools Used </h2>
            <Col style={{ flex: 1 }}>
                <Card style={{ width: '50%', textAlign: 'left' }}>
                    <Card.Body>
                        {toolsName.map((name, index) => (<Card.Text><b>{name}</b>{toolsDesc[index]}</Card.Text>))}
                    </Card.Body>
                </Card>
            </Col>
            <br />
            <br />
            <h2 style={{ textAlign: 'center' }}> API data sources: </h2>
            <Col style={{ flex: 1 }}>
                <Card style={{ width: '50%', textAlign: 'center' }}>
                    <Card.Body>
                        {apiSrc.map((help, index) => (
                            <Card.Text> <a href={help}>
                                <img
                                    src="https://uxwing.com/wp-content/themes/uxwing/download/07-design-and-development/web-api.png"
                                    alt="linkedinIcon"
                                    style={{ height: '1em' }}
                                />
                                &nbsp;<u>{apiText[index]}</u>
                            </a></Card.Text>
                        ))}
                    </Card.Body>
                </Card>
            </Col>
        </>
    )
}
