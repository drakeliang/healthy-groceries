import React from 'react';
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Highlight from 'react-highlighter';

/**
 * Creates card of given store with search terms highlighted
 *
 * @param {object} store fuse object containing data and search
 * 
 * @return JSX of the store card with search terms highlighted
 */
function StoreCardHighlight(store) {
    const searchTerm = store.matches[0].value
    store = store.item
    // choose correct image to display
    // TODO: move this to backend (maybe)
    const img = store.name === 'Shell Company' ? process.env.PUBLIC_URL + 'images/shell.jpg' : process.env.PUBLIC_URL + 'images/kroger.jpeg'

    return (
        <Card style={{ minWidth: '18rem', maxWidth: '21rem' }}>
            <Card.Img variant="top" src={img} />
            <Card.Title as="h3" style={{ padding: '5px', textAlign: 'center' }}><Highlight search={searchTerm || ""}>{store.name}</Highlight></Card.Title>
            <Card.Body style={{ fontSize: 15 }}>
                <Card.Text><Highlight search={searchTerm || ""}>{store.addressLine1}</Highlight></Card.Text>
                <Card.Text>
                    <Highlight search={searchTerm || ""}>{store.city}</Highlight>,&nbsp;
                    <Highlight search={searchTerm || ""}>{store.state}</Highlight>&nbsp;
                    <Highlight search={searchTerm || ""}>{store.zipCode}</Highlight>
                </Card.Text>
                <Link to={`${process.env.PUBLIC_URL}/grocery/${store.id}`}><Button style={{ margin: '0 auto', display: 'block' }} variant="primary">View Store</Button></Link>
            </Card.Body>
        </Card>
    )
}

export { StoreCardHighlight }