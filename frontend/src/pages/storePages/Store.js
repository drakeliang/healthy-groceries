import React from 'react';
import { Button, Container, Col, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import useAxios from 'axios-hooks'
import FoodsGrid from "../foodPages/FoodsGrid";
import ConditionsGrid from "../healthPages/ConditionsGrid";
import Map from './Map'
import Loading from '../../utils/Loading'

/**
  * Store instance page
  * 
  * @param {object} match object that contains store instance id
  * 
  * @return Store instance page
  */
export default function Store({ match, location }) {
    const { params: { groceryId } } = match;

    const [{ data, loading, error }] = useAxios(
        `/api/stores/${groceryId}`
    )
    const [{ data: foodData, loading: floading, error: ferror }] = useAxios(
        `/api/foods`
    )
    const [{ data: conditionData, loading: cloading, error: cerror }] = useAxios(
        `/api/conditions`
    )
    if (loading || floading || cloading) return <Loading/>
    if (error || ferror || cerror) return <p>Something went wrong</p>

    // choose correct image to display
    // TODO: move this to backend (maybe)
    const img = data.name === 'Shell Company' ? process.env.PUBLIC_URL + 'images/shell.jpg' : process.env.PUBLIC_URL + 'images/kroger.jpeg'

    return (
        <Container>
            <Row>
                <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <h1>{data.name}</h1>
                    <h4 style={{ textAlign: 'center' }}>{data.chain}</h4>
                    <h4 style={{ textAlign: 'center' }}>{data.addressLine1}</h4>
                    <h4 style={{ textAlign: 'center' }}>{data.city}, {data.state} {data.zipCode}</h4>
                    <br />
                    <h4 style={{ textAlign: 'center' }}>Latitude: {data.latitude}, Longitude: {data.longitude}</h4>
                    <h4 style={{ textAlign: 'center' }}>Store Hours: {data.hours}</h4>
                    <br />
                </Col>
                <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Map lat={data.latitude} lng={data.longitude} name={data.name} />
                </Col>
            </Row>
            <img src={img} className='center' alt=""></img>
            <br />
            <Link to={`${process.env.PUBLIC_URL}/grocery`}>
                <Button style={{ margin: '0 auto', display: 'block' }} variant="primary">
                    Back to Grocery Stores
                </Button>
            </Link>
            <br />
            <h1>Foods</h1>
            <FoodsGrid data={foodData} query={data.name.toLowerCase().includes("shell") ? "shell" : "kroger"} />
            <br />
            <h1>Health Conditions</h1>
            <ConditionsGrid data={conditionData} query={data.name.toLowerCase().includes("shell") ? "shell" : "kroger"} />
        </Container>
    )
}