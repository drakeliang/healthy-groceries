import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import useAxios from 'axios-hooks'

import StoresGrid from './StoresGrid'
import Loading from '../../utils/Loading'


export default function Stores() {
    const [query, updateQuery] = useState('');

    const [{ data, loading, error }] = useAxios(
        `/api/stores`
    )
    if (loading) return <Loading/>
    if (error) return <p>Something went wrong</p>

    /**
    * Gets a binary predicate to use in filtering in the form of a lambda.
    *
    * @param currentTarget contents of sitewide search bar to pass to Search
    */
    function onSearch({ currentTarget }) {
        updateQuery(currentTarget.value);
    }

    return (
        <React.Fragment>
            <Container>
                <Row>
                    <Col><h1>Grocery Stores</h1></Col>
                    <Col style={{ justifyContent: "center", display: "flex" }}>
                        <input type="text" placeholder="Search for Stores Here!" value={query} onChange={onSearch} />
                    </Col>
                </Row>
            </Container>
            <StoresGrid data={data} query={query} />
        </React.Fragment>
    )
}
