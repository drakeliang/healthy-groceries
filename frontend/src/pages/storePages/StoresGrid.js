import { useState } from 'react';
import { StoreCardHighlight } from './StoreCardHighlight'
import { FilterType, } from '../../utils/Transforms'
import { OneFilter } from '../../utils/OneFilter'
import { applyTransforms } from '../../utils/ApplyTransforms'
import { getGrid } from '../../utils/Grid'

export default function StoresGrid({ data, query }) {
    // get filter specification states
    const [page, setPage] = useState(1);
    const [sortField, setSortField] = useState("");
    const [sortAsc, setSortAsc] = useState(true);
    const [ , setFilterFlag] = useState(0.0);

    // keys to search by
    const fuseKeys = ['name', 'addressLine1', 'city']

    const optOut = '-';
    // possible values, possible operators, and whether to show the operator for each field
    const filterFieldsValues = {
        'chain': ['KROGER', 'SHELL COMPANY'],
        'zipCode': ['72002', '72209', '90011', '77006', '90003', '90037', '38116', '90001',
            '45206', '43068', '38118', '72015', '75001', '77007', '30310', '90002', '45202',
            '72019', '41071', '38141', '90255', '30316', '72223', '41073', '30303', '43230',
            '38654', '80013', '77003', '72210', '30306', '30309', '77023', '80237', '38672',
            '77026', '30312', '38637', '75248', '75006', '30308', '75287', '30318', '80012',
            '80014', '43213', '72022', '41074', '75230', '38671'],
        'state': ['AR', 'CA', 'CO', 'GA', 'KY', 'MS', 'OH', 'TN', 'TX'],
        'hours': ['24 hours', '6:00 - 23:59']
    };
    const filterFields = Object.keys(filterFieldsValues);
    const filterFieldsOps = {
        'chain': [FilterType.EQ],
        'zipCode': [FilterType.EQ],
        'state': [FilterType.EQ],
        'hours': [FilterType.EQ]
    };
    const filterFieldsShowOp = {
        'chain': false,
        'zipCode': false,
        'state': false,
        'hours': false
    }

    // create filters
    const [filters, ] = useState([]);
    filterFields.forEach((x) => filters.push(new OneFilter(setFilterFlag, x, filterFieldsOps[x][0])));

    const sortFieldsAliases = {
        'name': 'name',
        'latitude': 'latitude',
        'longitude': 'longitude'
    }
    const sortFields = Object.keys(sortFieldsAliases)

    const pageSize = 6;

    // get max pages and result data
    const [maxPages, slicedData] = applyTransforms(data, query, fuseKeys, filters, sortField, sortAsc, page, pageSize)

    return getGrid(slicedData, filterFields, optOut, filters, filterFieldsValues,
        filterFieldsOps, filterFieldsShowOp, sortField, sortFields, sortFieldsAliases,
        setSortField, sortAsc, setSortAsc, page, setPage, maxPages, StoreCardHighlight)
}
