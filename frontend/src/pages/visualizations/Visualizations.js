import React from 'react';
import { ConditionsVis, StoresVis, FoodsVis, MuseumVis } from './VisA';
import ArtworkVis from './ArtworkVis';
import ArtistsVis from './ArtistsVis';
import { Container, Card, Row, Col } from 'react-bootstrap'

export default function Visualizations() {
    return (
        <div>
            <h1>Our Visualizations</h1>
            <Container>
                <Row justify="center">
                    <Col>
                        <Card style={{ height: "500px", width: "600px" }}>
                            <Card.Header as="h4" style={{ textAlign: 'center' }}>
                                Number of Stores per State
                            </Card.Header>
                            <div style={{ height: "440px", width: "600px" }}>
                                <StoresVis />
                            </div>
                        </Card>
                        <br />
                        <Card style={{ height: "500px", width: "600px" }}>
                            <Card.Header as="h4" style={{ textAlign: 'center' }}>
                                Carbs vs. Calories
                            </Card.Header>
                            <div style={{ height: "440px", width: "600px" }}>
                                <FoodsVis />
                            </div>
                        </Card>
                        <br />
                        <Card style={{ height: "500px", width: "600px" }}>
                            <Card.Header as="h4" style={{ textAlign: 'center' }}>
                                Estimated Number of Yearly Cases per Condition
                            </Card.Header>
                            <div style={{ height: "440px", width: "600px" }}>
                                <ConditionsVis />
                            </div>
                        </Card>
                    </Col>
                </Row>
            </Container>
            <br />
            <h1>ArtLookup Visualizations</h1>
            <Container>
                <Row justify="center">
                    <Col>
                        <Card style={{ height: "500px", width: "600px" }}>
                            <Card.Header as="h4" style={{ textAlign: 'center' }}>
                                Number of Reviews per Museum
                            </Card.Header>
                            <div style={{ height: "440px", width: "600px" }}>
                                <MuseumVis />
                            </div>
                        </Card>
                    </Col>
                </Row>
                <br />
                <Row justify="center">
                    <Col>
                        <Card style={{ height: "500px", width: "600px" }}>
                            <Card.Header as="h4" style={{ textAlign: 'center' }}>
                                Number of Artworks Over Time
                            </Card.Header>
                            <div style={{ height: "440px", width: "600px" }}>
                                <ArtworkVis />
                            </div>
                        </Card>
                        <Card style={{ height: "500px", width: "600px" }}>
                            <Card.Header as="h4" style={{ textAlign: 'center' }}>
                                Most Common Nationalities Among Artists
                            </Card.Header>
                            <div style={{ height: "440px", width: "600px" }}>
                                <ArtistsVis />
                            </div>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>

    );
}

