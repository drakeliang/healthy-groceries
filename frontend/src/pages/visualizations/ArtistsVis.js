import { ResponsivePie } from '@nivo/pie';
import React from 'react';
import useAxios from 'axios-hooks';
import Loading from '../../utils/Loading';

export default function ArtistsVis() {

    const [{ data, loading, error }] = useAxios(
        `https://cors-anywhere.herokuapp.com/https://artlookup.me/api/Artists`
    )
    if (loading) return <Loading />
    if (error) return <p>Something went wrong</p>

    if (!data || data.length === 0) {
        return <Loading />;
    }

    let pieData = [];
    let nations = new Map();
    for (let artist of data.objects) {
        if (artist.hasOwnProperty('nationality')) {
            let nat = artist.nationality;
            if (nat.length > 0) {
                if (nations.has(nat)) {
                    nations.set(nat, nations.get(nat) + 1);
                }
                else {
                    nations.set(nat, 1);
                }
            }
        }
    }

    const nationsSorted = new Map([...nations.entries()].sort((a, b) => b[1] - a[1]));

    nationsSorted.forEach((value, key) => {
        if ( value > 2) {
            pieData.push({
                "id": key,
                "label": key,
                "value": value
            });
        }
    });

    return (
        <ResponsivePie
            data={pieData}
            margin={{
                left: 30,
                right: 30,
                top: 30,
                bottom: 30
            }}
        />
    );
}
