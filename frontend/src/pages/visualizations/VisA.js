import { ResponsiveTreeMap } from "@nivo/treemap";
import { ResponsiveScatterPlot } from "@nivo/scatterplot";
import { ResponsiveBar } from '@nivo/bar'
import { ResponsiveBubble } from '@nivo/circle-packing'
import React, { useState } from 'react';
import useAxios from 'axios-hooks'
import Loading from '../../utils/Loading'

function ConditionsVis() {
    const [query, updateQuery] = useState('');

    const [{ data, loading, error }] = useAxios(
        `/api/conditions`
    )

    if (loading) return <Loading />
    if (error) return <p>Something went wrong</p>

    if (!data || data.length === 0) {
        return <Loading />;
    }

    let treeData = {
        root: {
            name: "Yearly Cases",
            color: "hsl(305, 70%, 50%)",
            children: [],
        },
    };

    for (let cases of data) {
        treeData.root.children.push({
            name: cases.name,
            value: cases.cases,
        });
    }

    let thresh = 4000000;
    treeData.root.children = treeData.root.children.filter(
        (node) => node.value >= thresh
    );

    return (
        <ResponsiveTreeMap
            data={treeData.root}
            identity="name"
            value="value"
            label="id"
            enableParentLabel={false}
        />
    );
}

function StoresVis() {
    const [query, updateQuery] = useState('');

    const [{ data, loading, error }] = useAxios(
        `/api/stores`
    )

    if (loading) return <Loading />
    if (error) return <p>Something went wrong</p>

    if (!data || data.length === 0) {
        return <Loading />;
    }


    let states = ['AR', 'CA', 'CO', 'GA', 'KY', 'MS', 'OH', 'TN', 'TX'];
    let statesStruct = {};

    for (let x of states) {
        statesStruct[x] = {
            state: x,
            kroger: 0,
            krogerColor: "hsl(176, 70%, 50%)",
            shell: 0,
            shellColor: "hsl(98, 70%, 50%)",
            other: 0,
            otherColor: "hsl(67, 70%, 50%)",
        };
    }

    for (let x of data) {
        let naem = x.name.toLowerCase()
        if(naem.includes("kroger")) {
            statesStruct[x.state]["kroger"] += 1;
        } else if (naem.includes("shell")){
            statesStruct[x.state]["shell"] += 1;
        } else {
            statesStruct[x.state]["other"] += 1;
        }
    }

    let barData = [];
    for (let x of states) {
        barData.push(statesStruct[x]);
    }


    return (
        <ResponsiveBar
        data={barData}
        keys={[ 'kroger', 'shell', 'other']}
        indexBy="state"
        margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
        padding={0.3}
        valueScale={{ type: 'linear' }}
        indexScale={{ type: 'band', round: true }}
        colors={{ scheme: 'nivo' }} 
        borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'State',
            legendPosition: 'middle',
            legendOffset: 32
        }}
        axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'Number of Stores',
            legendPosition: 'middle',
            legendOffset: -40
        }}
        labelSkipWidth={12}
        labelSkipHeight={12}
        labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
        legends={[
            {
                dataFrom: 'keys',
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 120,
                translateY: 0,
                itemsSpacing: 2,
                itemWidth: 100,
                itemHeight: 20,
                itemDirection: 'left-to-right',
                itemOpacity: 0.85,
                symbolSize: 20,
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemOpacity: 1
                        }
                    }
                ]
            }
        ]}
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        />
    );
}

function FoodsVis() {
    const [query, updateQuery] = useState('');

    const [{ data, loading, error }] = useAxios(
        `/api/foods`
    )

    if (loading) return <Loading />
    if (error) return <p>Something went wrong</p>

    if (!data || data.length === 0) {
        return <Loading />;
    }

    var data2 = [];
    for (let x of data) {
        data2.push({
            x: x.calories,
            y: x.carbs
        });
    }

    data2 = data2.filter(
        (x) => x.x < 500 && x.y < 500
    );

    return (
        <ResponsiveScatterPlot
            data={[{id: 'foods', data: data2}]}
            margin={{ top: 60, right: 140, bottom: 70, left: 90 }}
            xScale={{ type: 'linear', min: -5, max: 'auto' }}
            xFormat={(x) => x + ' cal'}
            yScale={{ type: 'linear', min: -5, max: 'auto' }}
            yFormat={(y) => y + 'g'}
            axisBottom={{
                orient: 'bottom',
                legend: 'Calories',
                legendPosition: 'middle',
                legendOffset: 46
            }}
            axisLeft={{
                orient: 'left',
                legend: 'Carbs',
                legendPosition: 'middle',
                legendOffset: -60
            }}
            colors={{"scheme":"category10"}}
        />
    );
}

function MuseumVis() {
    const [query, updateQuery] = useState('');

    const [{ data, loading, error }] = useAxios(
        `https://cors-anywhere.herokuapp.com/https://artlookup.me/api/Museums`
    )

    if (loading) return <Loading />
    if (error) return <p>Something went wrong</p>

    if (!data || data.length === 0) {
        return <Loading />;
    }

    let museumData = {
        root: {
            name: "Number of Museum Ratings",
            color: "hsl(305, 70%, 50%)",
            children: [],
        },
    };

    for (let museum of data.objects) {
        museumData.root.children.push({
            name: museum.name,
            value: museum.num_ratings,
        });
    }

    let thresh = 20000;
    museumData.root.children = museumData.root.children.filter(
        (node) => node.value >= thresh
    );

    return (
        <ResponsiveBubble
            root={museumData.root}
            identity="name"
            value="value"
            leavesOnly={true}
            colorBy="name"
        />
    );
}

export { ConditionsVis, StoresVis, FoodsVis, MuseumVis }
