import { ResponsiveLine } from '@nivo/line'
import React, { useState } from 'react';
import useAxios from 'axios-hooks'
import Loading from '../../utils/Loading'

export default function ArtworkVis() {
    const [{ data, loading, error }] = useAxios(
        `https://cors-anywhere.herokuapp.com/https://artlookup.me/api/Artwork`
    )

    if (loading) return <Loading />
    if (error) return <p>Something went wrong</p>

    if (!data || data.length === 0) {
        return <Loading />;
    }

    let dates = [1200, 1250, 1300, 1350, 1400, 1450, 1500, 1550, 1600, 1650, 1700, 1750, 1800, 1850, 1900, 1950, 2000];
    let datesMap = {};
    
    for (let x of dates) {
        datesMap[x] = 0;
    }

    let total = 0;

    for (let x of data.objects) {
        let temp = parseInt(x.completion_year);
        temp = temp - temp % 50;
        datesMap[temp] += 1;
    }

    let artData = []

    for (let yr of dates) {
        total += datesMap[yr];
        artData.push({
            x: yr,
            y: total
        })
    }

    console.log(artData)

    let nivoData = [
        {
            id: "artworks",
            color: "hsl(92, 70%, 50%)",
            data: artData
        }
    ]

    return (
        <ResponsiveLine
        data={nivoData}
        margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
        xScale={{ type: 'point' }}
        yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false }}
        yFormat=" >-.2f"
        axisTop={null}
        axisRight={null}
        axisBottom={{
            orient: 'bottom',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'Year',
            legendOffset: 36,
            legendPosition: 'middle'
        }}
        axisLeft={{
            orient: 'left',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'Number of Artworks',
            legendOffset: -40,
            legendPosition: 'middle'
        }}
        pointSize={10}
        pointColor={{ theme: 'background' }}
        pointBorderWidth={2}
        pointBorderColor={{ from: 'serieColor' }}
        pointLabelYOffset={-12}
        useMesh={true}
        legends={[
            {
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 100,
                translateY: 0,
                itemsSpacing: 0,
                itemDirection: 'left-to-right',
                itemWidth: 80,
                itemHeight: 20,
                itemOpacity: 0.75,
                symbolSize: 12,
                symbolShape: 'circle',
                symbolBorderColor: 'rgba(0, 0, 0, .5)',
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemBackground: 'rgba(0, 0, 0, .03)',
                            itemOpacity: 1
                        }
                    }
                ]
            }
        ]}
    />
    )
}