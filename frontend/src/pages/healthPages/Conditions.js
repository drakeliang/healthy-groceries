import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import useAxios from 'axios-hooks'
import ConditionsGrid from './ConditionsGrid'
import Loading from '../../utils/Loading'

export default function Conditions() {
    const [query, updateQuery] = useState('');

    const [{ data, loading, error }] = useAxios(
        `/api/conditions`
    )
    if (loading) return <Loading/>
    if (error) return <p>Something went wrong</p>

    /**
    * Gets a binary predicate to use in filtering in the form of a lambda.
    *
    * @param currentTarget contents of sitewide search bar to pass to Search
    */
    function onSearch({ currentTarget }) {
        updateQuery(currentTarget.value);
    }

    return (
        <React.Fragment>
            <Container>
                <Row>
                    <Col><h1>Health Conditions</h1></Col>
                    <Col style={{ justifyContent: "center", display: "flex" }}>
                        <input type="text" placeholder="Search for Conditions Here!" value={query} onChange={onSearch} />
                    </Col>
                </Row>
            </Container>
            <ConditionsGrid data={data} query={query} />
        </React.Fragment>
    )
}
