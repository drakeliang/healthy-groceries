import { useState } from 'react';
import { ConditionCardHighlight } from './ConditionCardHighlight'
import { FilterType, } from '../../utils/Transforms'
import { OneFilter } from '../../utils/OneFilter'
import { applyTransforms } from '../../utils/ApplyTransforms'
import { getGrid } from '../../utils/Grid'

export default function ConditionsGrid({ data, query }) {
    // get filter specification states
    const [page, setPage] = useState(1);
    const [sortField, setSortField] = useState("");
    const [sortAsc, setSortAsc] = useState(true);
    const [ , setFilterFlag] = useState(0.0);

    // keys to search by
    const fuseKeys = ['store', 'name', 'avoid', 'consume']

    const optOut = '-';
    // possible values, possible operators, and whether to show the operator for each field
    const filterFieldsValues = {
        'influence': [1, 2, 3, 4, 5],
        'cases': [200000, 3000000, 20000],
        'avoid': ['Alcohol', 'Tobacco', 'Margarine & Cheese', 'Sweets', 'Caffeine', 'Fast Foods', 'Wheat'],
        'consume': ['Vegetables', 'Fish', 'Shellfish', 'Fruits']
    };
    const filterFields = Object.keys(filterFieldsValues);
    const filterFieldsOps = {
        'influence': [FilterType.EQ],
        'cases': [FilterType.EQ, FilterType.LT, FilterType.LTE, FilterType.GT, FilterType.GTE],
        'avoid': [FilterType.IN],
        'consume': [FilterType.IN]
    };
    const filterFieldsShowOp = {
        'influence': false,
        'cases': true,
        'avoid': false,
        'consume': false
    }

    // create filters
    const [filters, ] = useState([]);
    filterFields.forEach((x) => filters.push(new OneFilter(setFilterFlag, x, filterFieldsOps[x][0])));

    const sortFieldsAliases = {
        'name': 'name',
        'cases': 'cases',
        'influence': 'influence'
    }
    const sortFields = Object.keys(sortFieldsAliases)

    const pageSize = 6;

    // get max pages and result data
    const [maxPages, slicedData] = applyTransforms(data, query, fuseKeys, filters, sortField, sortAsc, page, pageSize)

    return getGrid(slicedData, filterFields, optOut, filters, filterFieldsValues,
        filterFieldsOps, filterFieldsShowOp, sortField, sortFields, sortFieldsAliases,
        setSortField, sortAsc, setSortAsc, page, setPage, maxPages, ConditionCardHighlight)
}