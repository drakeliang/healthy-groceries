import React from 'react';
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Highlight from 'react-highlighter';

/**
 * Creates card of given condition with search terms highlighted
 *
 * @param {object} condition fuse object containing data and search
 * 
 * @return JSX of the condition card with search terms highlighted
 */
function ConditionCardHighlight(condition) {
    // trim characters from search term if it is not undefined
    const searchTerm = condition.matches[0].value ? condition.matches[0].value.replace(/[[\]']+/g, '') : condition.matches[0].value;
    condition = condition.item
    var media = JSON.parse(condition.media.replace(/'/g, '"'))
    return (
        <Card style={{ minWidth: '18rem', maxWidth: '21rem' }}>
            <Card.Img variant="top" src={media[0]} />
            <Card.Body style={{ fontSize: 20 }}>
                <Card.Title as="h3" style={{ padding: '5px', textAlign: 'center' }}><Highlight search={searchTerm || ""}>{condition.name}</Highlight></Card.Title>
                <Card.Text>Avoid: <Highlight search={searchTerm || ""}>{condition.avoid.replace(/[[\]']+/g, '')}</Highlight></Card.Text>
                <Card.Text>Consume: <Highlight search={searchTerm || ""}>{condition.consume.replace(/[[\]']+/g, '')}</Highlight></Card.Text>
                <Link to={`${process.env.PUBLIC_URL}/health/${condition.id}`}>
                    <Button style={{ margin: '0 auto', display: 'block' }} variant="primary">View Condition</Button>
                </Link>
            </Card.Body>
        </Card>
    )
}

export { ConditionCardHighlight }