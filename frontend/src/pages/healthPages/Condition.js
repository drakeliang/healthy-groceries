import React from 'react';
import { Button, Container, Card, CardDeck } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import useAxios from 'axios-hooks'
import StoresGrid from "../storePages/StoresGrid";
import FoodsGrid from "../foodPages/FoodsGrid";
import Loading from '../../utils/Loading'

/**
  * Health Condition instance page
  * 
  * @param {object} match object that contains condition instance id
  * 
  * @return Health Condition instance page
  */
export default function Condition({ match, location }) {
    const { params: { conditionId } } = match;

    const [{ data, loading, error }] = useAxios(
        `/api/conditions/${conditionId}`
    )
    const [{ data: fdata, loading: floading, error: ferror }] = useAxios(
        `/api/foods`
    )
    const [{ data: sdata, loading: sloading, error: serror }] = useAxios(
        `/api/stores`
    )
    if (loading || floading || sloading) return <Loading/>
    if (error || ferror || serror) return <p>Something went wrong</p>

    //convert strings provided by endpoint into arrays of Strings
    var avoidReplaced = JSON.parse(data.avoid.replace(/'/g, '"'))
    var consumeReplaced = JSON.parse(data.consume.replace(/'/g, '"'))
    var media = JSON.parse(data.media.replace(/'/g, '"'))

    return (
        <Container>
            <h1>{data.name}</h1>
            <h2 style={{ textAlign: 'center' }}>Note: Please see a doctor for actual medical advice/assistance</h2>
            <br />
            <h3 style={{ textAlign: 'center' }}>Influence (how helpful dietary change will be): {data.influence} / 5</h3>
            <h3 style={{ textAlign: 'center' }}>Estimated Yearly Cases in the US: {data.cases}</h3>
            <br />
            <CardDeck style={{ flexWrap: 'wrap', justifyContent: 'center' }}>
                <Card style={{ minWidth: '18rem', maxWidth: '21rem' }}>
                    <Card.Title as="h3" style={{ padding: '5px', textAlign: 'center' }}>Foods to Avoid</Card.Title>
                    <Card.Body style={{ fontSize: 20 }}>
                        {avoidReplaced.map(FoodList)}
                    </Card.Body>
                </Card>
                <Card style={{ minWidth: '18rem', maxWidth: '21rem' }}>
                    <Card.Title as="h3" style={{ padding: '5px', textAlign: 'center' }}>Foods to Consume</Card.Title>
                    <Card.Body style={{ fontSize: 20 }}>
                        {consumeReplaced.map(FoodList)}
                    </Card.Body>
                </Card>
            </CardDeck>
            <br />
            <CardDeck style={{ flexWrap: 'wrap', justifyContent: 'center' }}>
                <Card>
                    <img src={media[1]} class='center' alt='' style={{
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                        marginTop: "auto",
                        marginBottom: "auto",
                        height: "auto",
                        width: "100%"
                    }}></img>
                </Card>
                <Card>
                    <img src={media[0]} class='center' alt='' style={{
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                        marginTop: "auto",
                        marginBottom: "auto",
                        height: "auto",
                        width: "100%"
                    }}></img>
                </Card>
            </CardDeck>
            <br />
            <Link to={`${process.env.PUBLIC_URL}/health`}>
                <Button style={{ margin: '0 auto', display: 'block' }} variant="primary">
                    Back to Health Conditions
                </Button>
            </Link>
            <br />
            <h1>Foods</h1>
            <FoodsGrid data={fdata} query={avoidReplaced[0]} />
            <br />
            <h1>Grocery Stores</h1>
            <StoresGrid data={sdata} query={data.store === "kroger, shell" ? "" : "kroger"}/>
        </Container>
    )
}

function FoodList(food) {
    return (
        <h4>{food}</h4>
    )
}