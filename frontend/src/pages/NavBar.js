import React, { useState } from 'react';
import { Link, NavLink } from "react-router-dom"

export default function NavBar() {
    const [query, updateQuery] = useState('');

    /**
    * Gets a binary predicate to use in filtering in the form of a lambda.
    *
    * @param currentTarget contents of sitewide search bar to pass to Search
    */
    function onSearch({ currentTarget }) {
        updateQuery(currentTarget.value);
    }

    return (
        <>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/grocery">Grocery Stores</NavLink></li>
            <li><NavLink to="/food">Foods</NavLink></li>
            <li><NavLink to="/health">Conditions</NavLink></li>
            <li><NavLink to="/about">About</NavLink></li>
            <li><NavLink to="/visualizations">Visualizations</NavLink></li>
            <li className="navbar-right"><input type="text" placeholder="Sitewide Search" value={query} onChange={onSearch} /></li>
            <li><Link to={`${process.env.PUBLIC_URL}/search/${query}`}>Search</Link></li>
        </>
    )
}