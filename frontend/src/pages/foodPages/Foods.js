import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import useAxios from 'axios-hooks'
import FoodsGrid from './FoodsGrid'
import Loading from '../../utils/Loading'

export default function Foods() {

    const [query, updateQuery] = useState('');

    const [{ data, loading, error }] = useAxios(
        `/api/foods`
    )
    if (loading) return <Loading/>
    if (error) return <p>Something went wrong</p>

    /**
    * Gets a binary predicate to use in filtering in the form of a lambda.
    *
    * @param currentTarget contents of sitewide search bar to pass to Search
    */
    function onSearch({ currentTarget }) {
        updateQuery(currentTarget.value);
    }

    return (
        <>
            <Container>
                <Row>
                    <Col><h1>Foods</h1></Col>
                    <Col style={{ justifyContent: "center", display: "flex" }}>
                        <input type="text" placeholder="Search for Foods Here!" value={query} onChange={onSearch} />
                    </Col>
                </Row>
            </Container>
            <FoodsGrid data={data} query={query} />
        </>
    )
}