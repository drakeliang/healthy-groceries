import React from 'react';
import { Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Highlight from 'react-highlighter';

/**
 * Creates card of given food with search terms highlighted
 *
 * @param {object} food fuse object containing data and search
 * 
 * @return JSX of the food card with search terms highlighted
 */
function FoodCardHighlight(food) {
    const searchTerm = food.matches[0].value
    food = food.item
    return (
        <Card style={{ minWidth: '18rem', maxWidth: '21rem' }}>
            <Card.Img variant="top" src={food.image} style={{
                display: "block",
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: "auto",
                marginBottom: "auto",
                width: "75%",
                height: "auto"
            }} />
            <Card.Body style={{ fontSize: 20 }}>
                <Card.Title as="h3" style={{ padding: '5px', textAlign: 'center' }}><Highlight search={searchTerm || ""}>{food.name}</Highlight></Card.Title>
                <Card.Text>Food Group: <Highlight search={searchTerm || ""}>{food['food group']}</Highlight></Card.Text>
                <Card.Text>Calories: <Highlight search={searchTerm || ""}>{food['calories'].toFixed(2)}</Highlight></Card.Text>
                <Card.Text>Protein: <Highlight search={searchTerm || ""}>{food['protein'].toFixed(2)}g</Highlight></Card.Text>
                <Card.Text>Fat: <Highlight search={searchTerm || ""}>{food['fat'].toFixed(2)}g</Highlight></Card.Text>
                <Card.Text>Carbs: <Highlight search={searchTerm || ""}>{food['carbs'].toFixed(2)}g</Highlight></Card.Text>
                <Card.Text>Fiber: <Highlight search={searchTerm || ""}>{food['fiber'].toFixed(2)}g</Highlight></Card.Text>
                <Link to={`${process.env.PUBLIC_URL}/food/${food.id}`}>
                    <Button style={{ margin: '0 auto', display: 'block' }} variant="primary">View Food</Button>
                </Link>
            </Card.Body>
        </Card>
    )
}

export { FoodCardHighlight }