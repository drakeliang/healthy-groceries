import { useState } from 'react';
import { FoodCardHighlight } from './FoodCardHighlight'
import { FilterType, } from '../../utils/Transforms'
import { OneFilter } from '../../utils/OneFilter'
import { applyTransforms } from '../../utils/ApplyTransforms'
import { getGrid } from '../../utils/Grid'

export default function FoodsGrid({ data, query }) {
    // get filter specification states
    const [page, setPage] = useState(1);
    const [sortField, setSortField] = useState("");
    const [sortAsc, setSortAsc] = useState(true);
    const [ , setFilterFlag] = useState(0.0);

    // keys to search by
    const fuseKeys = ['food group', 'name', 'id', 'store']

    const optOut = '-';
    // possible values, possible operators, and whether to show the operator for each field
    const filterFieldsValues = {
        'food group': ['Margarine & cheese', 'Fruits', 'Wheat', 'Luncheon (processed) meats', 'Vegetables', 'Sweets', 'Fast foods'],
        'calories': [50, 100, 150, 200, 250, 300],
        'fat': [20, 40, 60, 80, 100],
        'carbs': [30, 60, 90, 120],
    };
    const filterFields = Object.keys(filterFieldsValues);
    const filterFieldsOps = {
        'food group': [FilterType.EQ],
        'calories': [FilterType.EQ, FilterType.LT, FilterType.LTE, FilterType.GT, FilterType.GTE],
        'protein': [FilterType.EQ, FilterType.LT, FilterType.LTE, FilterType.GT, FilterType.GTE],
        'fat': [FilterType.EQ, FilterType.LT, FilterType.LTE, FilterType.GT, FilterType.GTE],
        'carbs': [FilterType.EQ, FilterType.LT, FilterType.LTE, FilterType.GT, FilterType.GTE],
        'fiber': [FilterType.EQ, FilterType.LT, FilterType.LTE, FilterType.GT, FilterType.GTE],
    };
    const filterFieldsShowOp = {
        'food group': false,
        'calories': true,
        'protein': true,
        'fat': true,
        'carbs': true,
        'fiber': true
    }

    // create filters
    const [filters, ] = useState([]);
    filterFields.forEach((x) => filters.push(new OneFilter(setFilterFlag, x, filterFieldsOps[x][0])));

    const sortFieldsAliases = {
        'name': 'name',
        'calories': 'calories',
        'protein': 'protein',
        'fat': 'fat',
        'carbs': 'carbs',
        'fiber': 'fiber'
    }
    const sortFields = Object.keys(sortFieldsAliases)

    const pageSize = 6;

    // get max pages and result data
    const [maxPages, slicedData] = applyTransforms(data, query, fuseKeys, filters, sortField, sortAsc, page, pageSize)

    return getGrid(slicedData, filterFields, optOut, filters, filterFieldsValues,
        filterFieldsOps, filterFieldsShowOp, sortField, sortFields, sortFieldsAliases,
        setSortField, sortAsc, setSortAsc, page, setPage, maxPages, FoodCardHighlight)
}