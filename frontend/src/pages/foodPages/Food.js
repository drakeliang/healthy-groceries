import React from 'react';
import { Bar } from 'react-chartjs-2';
import { Button, Container, Card, CardDeck } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import useAxios from 'axios-hooks'
import StoresGrid from "../storePages/StoresGrid";
import ConditionsGrid from "../healthPages/ConditionsGrid"
import Loading from '../../utils/Loading'

/**
  * Food instance page
  * 
  * @param {object} match object that contains the id of food instance
  * 
  * @return Food instance page
  */
export default function Food({ match, location }) {
    const { params: { foodId } } = match;

    const [{ data, loading, error }] = useAxios(
        `/api/foods/${foodId}`
    )
    const [{ data: cdata, loading: cloading, error: cerror }] = useAxios(
        `/api/conditions`
    )
    const [{ data: sdata, loading: sloading, error: serror }] = useAxios(
        `/api/stores`
    )
    if (loading || cloading || sloading) return <Loading />
    if (error || cerror || serror) return <p>Something went wrong</p>

    // object for creating BarChart, contains food's data
    var nutritionData = {
        labels: ['Protein', 'Fat',
            'Carbs', 'Fiber'],
        datasets: [
            {
                label: 'Grams',
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgba(0,0,0,1)',
                borderWidth: 2,
                data: [data.protein, data.fat, data.carbs, data.fiber]
            }
        ]
    }

    return (
        <Container>
            <h1>{data.name}</h1>
            <h3 style={{ textAlign: 'center' }}>Food Group: {data['food group']}</h3>
            <CardDeck style={{ flexWrap: 'wrap', justifyContent: 'center' }}>
                <Card><img src={data.image} className='center' alt='' style={{
                    display: "block",
                    marginLeft: "auto",
                    marginRight: "auto",
                    marginTop: "auto",
                    marginBottom: "auto"
                }} /></Card>
                <Card style={{ minWidth: '18rem', maxWidth: '40rem' }}>
                    <Card.Title as="h3" style={{ padding: '5px', textAlign: 'center' }}>Nutrition Facts</Card.Title>
                    <Card.Body style={{ fontSize: 20 }}>
                        <h4>Calories: {data.calories}</h4>
                        <Bar
                            data={nutritionData}
                            options={{
                                title: {
                                    display: true,
                                    text: 'Food Nutrients',
                                    fontSize: 20
                                },
                                legend: {
                                    display: true,
                                    position: 'right'
                                }
                            }}
                        />
                        <h4>Associated Condition(s): <Link to={`${process.env.PUBLIC_URL}/health/${data.consume}`}>{data.consume}</Link></h4>

                    </Card.Body>
                </Card>
            </CardDeck>
            <br />
            <Link to={`${process.env.PUBLIC_URL}/food`}>
                <Button style={{ margin: '0 auto', display: 'block' }} variant="primary">
                    Back to Foods
                </Button>
            </Link>
            <br />
            <h1>Health Conditions</h1>
            <ConditionsGrid data={cdata} query={data['food group']} />
            <br />
            <h1>Grocery Stores</h1>
            <StoresGrid data={sdata} query={data.store === "kroger, shell" ? "" : "kroger"}/>
        </Container>
    )
}

