import React from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';

import './style.css';

export default function Home() {
    return (
        <Container>
            <Row>
                <Col><h1>Welcome to Healthy Groceries!</h1>
                    <p>
                        This website is dedicated to finding and comparing groceries around your area, whether for financial or health reasons.&nbsp;
                        You can browse local grocery stores for their inventory, look up nutrient facts of specific foods,&nbsp;
                        or even discover what foods you need to eat to help your health condition.
                        </p>
                </Col>
            </Row>
            <Row style={{ display: 'flex', flexDirection: 'row' }}>
                <Col style={{ flex: 1 }}>
                    <Card style={{ width: '18rem', textAlign: 'center' }}>
                        <Card.Img variant="top" src={process.env.PUBLIC_URL + 'images/grocery.png'} />
                        <Card.Body>
                            <Card.Title style={{ padding: '5px', textAlign: 'center' }}>Grocery Stores</Card.Title>
                            <Link to="/grocery"><Button style={{ margin: '0 auto', display: 'block' }} variant="primary">Explore</Button></Link>
                        </Card.Body>
                    </Card>
                </Col>
                <Col style={{ flex: 1 }}>
                    <Card style={{ width: '18rem', textAlign: 'center' }}>
                        <Card.Img variant="top" src={process.env.PUBLIC_URL + 'images/bread.jpg'} />
                        <Card.Body>
                            <Card.Title style={{ padding: '5px', textAlign: 'center' }}>Food</Card.Title>
                            <Link to="/Food"><Button style={{ margin: '0 auto', display: 'block' }} variant="primary">Explore</Button></Link>
                        </Card.Body>
                    </Card>
                </Col>
                <Col style={{ flex: 1 }}>
                    <Card style={{ width: '18rem', textAlign: 'center' }}>
                        <Card.Img variant="top" src={process.env.PUBLIC_URL + 'images/health.jpg'} />
                        <Card.Body>
                            <Card.Title style={{ padding: '5px', textAlign: 'center' }}>Health Conditions</Card.Title>
                            <Link to="/health"><Button style={{ margin: '0 auto', display: 'block' }} variant="primary">Explore</Button></Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

