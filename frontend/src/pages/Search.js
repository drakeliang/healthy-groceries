import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import useAxios from 'axios-hooks'
import ConditionsGrid from './healthPages/ConditionsGrid'
import FoodsGrid from './foodPages/FoodsGrid'
import StoresGrid from './storePages/StoresGrid'
import Loading from '../utils/Loading'

/**
  * Returns search results of sitewide search
  * 
  * @param {object} match object that contains the search query
  * 
  * @return JSX of search results
  */
export default function Search({ match, location }) {
    const { params: { query } } = match;

    const [{ data: storeData, loading: sloading, error: serror }] = useAxios(
        `/api/stores`
    )
    const [{ data: foodData, loading: floading, error: ferror }] = useAxios(
        `/api/foods`
    )
    const [{ data: conditionData, loading: cloading, error: cerror }] = useAxios(
        `/api/conditions`
    )
    if (sloading || floading || cloading) return <Loading/>
    if (serror || ferror || cerror) return <p>Something went wrong</p>

    // pass data and query to models' grid pages
    // construct JSX output using the grid pages
    return (
        <div>
            <div>
                <Container>
                    <Row>
                        <Col><h1>Sitewide Search For: {query}</h1></Col>
                    </Row>
                </Container>
            </div>
            <div>
                <Container>
                    <Row>
                        <Col><h1>Stores</h1></Col>
                    </Row>
                </Container>
                <StoresGrid data={storeData} query={query} />
            </div>
            <br />
            <div>
                <Container>
                    <Row>
                        <Col><h1>Foods</h1></Col>
                    </Row>
                </Container>
                <FoodsGrid data={foodData} query={query} />
            </div>
            <br />
            <div>
                <Container>
                    <Row>
                        <Col><h1>Conditions</h1></Col>
                    </Row>
                </Container>
                <ConditionsGrid data={conditionData} query={query} />
            </div>
            <br />
            <div>
                <Container>
                    <Row>
                        <Col>
                            <h3 style={{ textAlign: 'center' }}>
                                <a href={`${process.env.PUBLIC_URL}/Home`}> Results on Home Page </a>
                            </h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h3 style={{ textAlign: 'center' }}>
                                <a href={`${process.env.PUBLIC_URL}/About`}> Results on About Page </a>
                            </h3>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}