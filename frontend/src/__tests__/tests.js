import React from 'react'
import ReactDOM from 'react-dom';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from '../App'
import Home from '../pages/Home'
import Foods from '../pages/foodPages/Foods'
import Stores from '../pages/storePages/Stores'
import Conditions from '../pages/healthPages/Conditions'
import About from '../pages/about/About'
import Food from '../pages/foodPages/Food'
import Store from '../pages/storePages/Store'
import Condition from '../pages/healthPages/Condition'


// Use mock data for jest
const mockData = {}
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(mockData),
  })
);

// Mock props for cards 
const mockProps = { params: 0 };

configure({ adapter: new Adapter() });

describe('App', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App />, div);
    });

    test('renders page', () => {
        const test = shallow(<App />); 
        expect(test).toMatchSnapshot();
    });
});
describe('Home', () => {
    test('renders page', () => {
        const test = shallow(<Home />); 
        expect(test).toMatchSnapshot();
    });
});
describe('About', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<About />, div);
    });

    test('renders page', () => {
        const test = shallow(<About />); 
        expect(test).toMatchSnapshot();
    });
});
describe('Foods', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Foods />, div);
    });
    test('renders model', () => {
        const test = shallow(<Foods />); 
        expect(test).toMatchSnapshot();
    });
    it('renders instance page without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Food match={mockProps}/>, div);
    });
    it('renders instance page', () => {
        const test = shallow(<Foods match={mockProps}/>); 
        expect(test).toMatchSnapshot();
    });
});
describe('Health Conditions', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Conditions />, div);
    });
    test('renders model', () => {
        const test = shallow(<Conditions />); 
        expect(test).toMatchSnapshot();
    });
    it('renders instance page without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Condition match={mockProps}/>, div);
    });
    it('renders instance page', () => {
        const test = shallow(<Condition match={mockProps}/>); 
        expect(test).toMatchSnapshot();
    });
});
describe('Grocery Stores', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Stores />, div);
    });
    test('renders model', () => {
        const test = shallow(<Stores />); 
        expect(test).toMatchSnapshot();
    });
    it('renders instance page without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Store match={mockProps}/>, div);
    });
    it('renders instance page', () => {
        const test = shallow(<Store match={mockProps}/>); 
        expect(test).toMatchSnapshot();
    });
});
