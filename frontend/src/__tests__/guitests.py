import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestGUI(unittest.TestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(options=chrome_options)

    def test_title(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.healthygroceries.me/")
        self.assertEqual(self.driver.title, "Healthy Groceries")

    def test_stores_model(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.healthygroceries.me/")
        button = self.driver.find_element_by_xpath('//*[@id="root"]/div/ul/li[2]/a')
        button.click()
        stores_title = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div[1]/div/div[1]/h1'
        ).text
        self.assertEqual(stores_title, "Grocery Stores")

    def test_stores_instance(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://healthygroceries.me/#/grocery/1")
        store_title = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div/div[1]/div[1]/h1'
        ).text
        self.assertNotEqual(store_title, None)

    def test_foods_model(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.healthygroceries.me/")
        button = self.driver.find_element_by_xpath('//*[@id="root"]/div/ul/li[3]/a')
        button.click()
        foods_title = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div[1]/div/div[1]/h1'
        ).text
        self.assertEqual(foods_title, "Foods")

    def test_foods_instance(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.healthygroceries.me/#/food/1")
        foods_title = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div/h1[1]'
        ).text
        self.assertNotEqual(foods_title, None)

    def test_health_model(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.healthygroceries.me/")
        button = self.driver.find_element_by_xpath('//*[@id="root"]/div/ul/li[4]/a')
        button.click()
        health_title = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div[1]/div/div[1]/h1'
        ).text
        self.assertEqual(health_title, "Health Conditions")

    def test_health_instance(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.healthygroceries.me/#/health/1")
        health_title = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div/h1[1]'
        ).text
        self.assertNotEqual(health_title, None)

    def test_about(self):
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.healthygroceries.me/#/about")
        about_title = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div/h1'
        ).text
        self.assertEqual(about_title, "About HealthyGroceries.me")

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
