# FROM python:3

# RUN apt-get -y update
# RUN apt-get -y upgrade

# COPY requirements.txt requirements.txt
# RUN pip3 install -r requirements.txt

# CMD bash

# COPY . backend

# EXPOSE 8080

# WORKDIR backend

# ENV FLASK_APP test.py
# ENV FLASK_RUN_PORT 8080

# RUN cd flask

# ENTRYPOINT flask run --host=0.0.0.0

# CMD flask run

FROM nikolaik/python-nodejs

RUN ls && pwd

RUN git clone https://gitlab.com/drakeliang/healthy-groceries.git

WORKDIR /healthy-groceries

RUN ls && pwd
RUN cd frontend && npm install && npm run build

RUN pip3 install -r backend/requirements.txt
RUN git pull --force

RUN pip3 list

EXPOSE 5000

CMD git pull --force && python3 backend/flask/main.py

# docker build --rm --tag backend:0.1 .
# docker run -p 5000:5000 backend:0.1
