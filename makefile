BLACK         := black
CHECKTESTDATA := checktestdata
COVERAGE      := coverage3
MYPY          := mypy
PYDOC         := pydoc3
PYLINT        := pylint
PYTHON        := python3
PIP			  := pip

upgrade:
	$(PIP) install --upgrade pip

clean:

project.log:
	git log > project.log

all:

config:
	git config -l

docker:
	docker run -it -v $(pwd):/app -w /app -p 3000:3000 -e CHOKIDAR_USEPOLLING=true frontend

pull:
	make clean
	@echo
	git pull
	git status

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

js-tests:
	cd frontend; \
	npm install; \
	npm test

selenium-tests:
	cd frontend; \
	pip install selenium; \
	python3 src/__tests__/guitests.py

api-tests:
	cd backend/flask; \
	python3 main-tests.py
